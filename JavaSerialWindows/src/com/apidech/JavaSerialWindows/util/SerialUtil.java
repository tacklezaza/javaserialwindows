package com.apidech.JavaSerialWindows.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.apidech.JavaSerialWindows.SerialPortNumber;
import com.apidech.JavaSerialWindows.SerialWindowsMain;

public class SerialUtil {
	public synchronized static void sendSerial(SerialPortNumber port, String message){
		OutputStream out = SerialWindowsMain.saveSerialPort.get(port).getOutputStream();
		try {
			out.write(message.getBytes());
			out.write('|');
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String getAPI(String urlToRead) throws IOException {
		StringBuilder result = new StringBuilder();
	    URL url = new URL(urlToRead);// "http://example.com/xxx"
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setUseCaches(false);
	    conn.setDefaultUseCaches(false);
	    conn.setRequestMethod("GET");
	    conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB;     rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 (.NET CLR 3.5.30729)");
	    BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	    String line;
	    while ((line = rd.readLine()) != null) {
	       result.append(line);
	    }
	    rd.close();
	    return result.toString();
	}
}
