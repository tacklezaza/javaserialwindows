package com.apidech.JavaSerialWindows;

public enum SerialPortNumber {
	ARDUINONANO_A("COM8"),
	ARDUINONANO_B("COM9");
	//ARDUINONANO("COM40");
	
	private String serialPortName;
	private SerialPortNumber(String serialPortName) {
		this.serialPortName = serialPortName;
	}
	public String getSerialPortName() {
		return this.serialPortName;
	}
}
