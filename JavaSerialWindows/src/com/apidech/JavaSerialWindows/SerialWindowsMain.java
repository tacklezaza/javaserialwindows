package com.apidech.JavaSerialWindows;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import com.apidech.JavaSerialWindows.util.SerialReader;
import com.apidech.JavaSerialWindows.util.SerialUtil;
import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;

public class SerialWindowsMain {

	public static HashMap<SerialPortNumber, SerialPort> saveSerialPort = new HashMap<>();
	private static SerialPortListener listener = new SerialPortListener();

	public static void main(String[] args) throws InterruptedException {
		for (SerialPortNumber serial : SerialPortNumber.values()) {
			SerialPort port = SerialPort.getCommPort(serial.getSerialPortName());
			System.out.println("Starting port: "+serial.getSerialPortName());
			if (port.openPort()) {
				System.out.println("Successfully open port: "+serial.getSerialPortName());
				saveSerialPort.put(serial, port);
				port.addDataListener(new SerialPortDataListener() {
					
					private SerialReader serialReader = new SerialReader();
					
					@Override
					public int getListeningEvents() {
						return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
					}

					@Override
					public void serialEvent(SerialPortEvent event) {
						if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
							return;
						
						while(port.bytesAvailable() > 0) {
							byte[] newData = new byte[1];
							port.readBytes(newData, newData.length);
							String toString = new String(newData);
							serialReader.appendSerial(toString);
							
							String line = serialReader.getLine();
							if(line != null) {
								listener.OnPortListener(serial, line);
							}
						}
					}
				});
			}
			else {
				System.out.println("Error! "+serial.name()+" on "+serial.getSerialPortName()+" is not Open!");
				System.out.println("Or other Java process is still running, kill it first!");
				System.out.println("Exiting . . .");
				Thread.sleep(2000);
				System.exit(0);
			}
		}
		//Listener for Text on Console
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Welcome to Express console");
		System.out.println("=================================");
		System.out.println("To send command type: [port] [message]");
		System.out.println("Example: COM8 Hello World");
		System.out.println("");
		try {
			while(true) {
				String input = reader.readLine();
				String[] split = input.split(" ", 2);
				
				if(split.length < 2) {
					System.out.println("Used: [Serial port (Ex. 'COM8')] [Message]");
					continue;
				}
				
				SerialPortNumber sendToPort = null;
				for (SerialPortNumber port : SerialPortNumber.values()) {
					if(port.getSerialPortName().equalsIgnoreCase(split[0])) {
						sendToPort = port;
						break;
					}
				}
				if(sendToPort != null) {
					SerialUtil.sendSerial(sendToPort, split[1]);
				}
				else {
					System.out.println("Port '"+split[0]+"' is not found");
				}
			}
		}
		catch (Exception e) {}
	}
}
