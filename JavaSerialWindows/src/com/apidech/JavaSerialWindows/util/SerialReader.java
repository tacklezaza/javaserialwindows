package com.apidech.JavaSerialWindows.util;

public class SerialReader {
	private StringBuilder sb;
	private char eom = '|';
	public SerialReader() {
		sb = new StringBuilder();
	}
	
	public void appendSerial(String s) {
		sb.append(s);
	}
	
	public String getLine() {
		while(sb.length() > 0) {
			if(sb.charAt(0) == this.eom){//Some thing gone wrong with EOM remove it!
				this.sb.deleteCharAt(0);
				continue;
			}
			String toString = sb.toString();
			int foundEOM = toString.indexOf(this.eom);
			if(foundEOM > -1) {
				String toReturn = sb.substring(0, foundEOM);
				sb.delete(0, foundEOM+1);
				return toReturn;
			}
			return null;
		}
		return null;
	}
}
